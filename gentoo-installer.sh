#!/bin/bash
# You can either generate a configuration file, or install with an already-generated file.
echo $(dialog --stdout --defaultno --title "Gentoo Installer" --yesno "Welcome to Max's Gentoo install helper.\n\nThis script asks a series of questions about your computer hardware and preferences for your system, spitting out a personal configuration file. This file can then be used to install Gentoo to your specifications on your particular hardware.\n\nWould you like to install Gentoo with an existing configuration file?" 15 60)

