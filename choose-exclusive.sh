#!/bin/bash
cmd=(dialog --keep-tite --menu "Select options:" 22 76 16)

options=(1 "Option 1"
         2 "Option 2"
         3 "Option 3"
         4 "Option 4")

choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)

for choice in $choices
do
    case $choice in
        1)
            echo "First Option"
            ;;
        2)
            echo "Second Option"
            ;;
        3)
            echo "Third Option"
            ;;
        4)
            echo "Fourth Option"
            ;;
    esac
done

